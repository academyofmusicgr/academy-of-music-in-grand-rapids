The Academy of Music in Grand Rapids is the largest professional music lesson studio in the Grand Rapids area offering quality lessons in Piano, Electric & Acoustic Guitar, Drums, Bass, Ukulele, Voice and Singing, Violin, Viola, and Cello all taught under one roof by expert instructors.

Address: 6159 28th St SE, #24, Grand Rapids, MI 49546, USA

Phone: 616-965-1655

Website: https://www.academyofmusicgr.com